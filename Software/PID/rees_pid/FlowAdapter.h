#ifndef FLOW_ADAPTER_H
#define FLOW_ADAPTER_H

class FlowAdapter
{
  public:
    FlowAdapter(void);
    void init(void);
    float readFlow(void);

  private:
};

#endif
